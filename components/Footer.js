const Footer = () => (
<div>

<footer class="page-footer font-small unique-color-dark pt-4 bg-dark">
              
              <div class="container ">  
                <ul class="list-unstyled list-inline text-center py-2">
                  <li class="list-inline-item">
                    <h5 class="mb-1 text-light">Footer</h5>
                  </li>
                  <li class="list-inline-item text-light">
                    <p>testi</p>
                  </li>
                </ul>
              </div>
    </footer>
          
</div>

);

export default Footer;