import styles from '../pages/Style.module.css';

const Field  = () => (

<table className={styles.table}>
    <tr className={styles.tr}> {/*rivi 1*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 2*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 3*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 4*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 5*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 6*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 7*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 8*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 9*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 10*/}  
        <td className={styles.td}>1</td> {/*solu*/}
        <td className={styles.td}>2</td>
        <td className={styles.td}>3</td>
        <td className={styles.td}> 4</td>
        <td className={styles.td}>5</td>
        <td className={styles.td}>6</td>
        <td className={styles.td}>7</td>
        <td className={styles.td}>8</td>
        <td className={styles.td}>9</td>
        <td className={styles.td}>10</td>
    </tr>

 

</table>


);

export default Field;