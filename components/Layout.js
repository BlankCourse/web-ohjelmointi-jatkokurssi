import Navbar from './Navbar';
import Head from "next/head";
import Field  from './Field';
import Footer from './Footer';
import Spacer from './Spacer';
import styles from '../pages/Style.module.css';

const Layout = (props) => (

    <div>
        
        {/*Vaihtaa sivun title*/}
        <Head>
            <title>Laivanupotus</title>
        
        {/*Bootstrap*/}
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"/>
        {/*jquery*/}
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        {/*javascript*/}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        {/*style sheet*/}
        <link rel="stylesheet" href="App.css" type="text/css"/>

        
        </Head>

         {/*Header*/}
    <body className={styles.body}>
        <main className={styles.main}> 

    
            <Navbar/> 
            <Spacer/> {/*blokki millä asetellut sivun elementit paremmin*/}
        
            <div class="container"> 
                
                <div class="row">
                    <h1 class="col">Pelaaja 1</h1>
                    <h1 class="col">Pelaaja 2</h1>
                </div>

                <div class="row">   {/*asettaa kentät riviin*/}
                    <div class="col">
                        <Field/> {/*pelikenttä*/} 
                    </div>
                   
                    <div class="col">
                        <Field/> {/*pelikenttä*/} 
                    </div>
                </div>

            </div>


            <Spacer/>
            <Spacer/>
            <Spacer/>
        
            <Footer/>

        </main>
    </body>
    </div>
);

export default Layout;