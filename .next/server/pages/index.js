module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Field.js":
/*!*****************************!*\
  !*** ./components/Field.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pages/Style.module.css */ "./pages/Style.module.css");
/* harmony import */ var _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\components\\Field.js";


const Field = () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("table", {
  className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.table,
  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 32,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 45,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 59,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 58,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 71,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 90,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 92,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 94,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 84,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 100,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 107,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 97,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 116,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 117,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 119,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 120,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 110,
    columnNumber: 5
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("tr", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tr,
    children: [" ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "1"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 124,
      columnNumber: 9
    }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "2"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 125,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "3"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 126,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: " 4"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 127,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "5"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 128,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "6"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 129,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "7"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 130,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "8"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 131,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "9"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 132,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("td", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.td,
      children: "10"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 133,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 123,
    columnNumber: 5
  }, undefined)]
}, void 0, true, {
  fileName: _jsxFileName,
  lineNumber: 5,
  columnNumber: 1
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Field);

/***/ }),

/***/ "./components/Footer.js":
/*!******************************!*\
  !*** ./components/Footer.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\components\\Footer.js";

const Footer = () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("footer", {
    class: "page-footer font-small unique-color-dark pt-4 bg-dark",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      class: "container ",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
        class: "list-unstyled list-inline text-center py-2",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
          class: "list-inline-item",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h5", {
            class: "mb-1 text-light",
            children: "Footer"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 9,
            columnNumber: 21
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 8,
          columnNumber: 19
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
          class: "list-inline-item text-light",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("p", {
            children: "testi"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 12,
            columnNumber: 21
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 11,
          columnNumber: 19
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 17
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 15
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 4,
    columnNumber: 1
  }, undefined)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 2,
  columnNumber: 1
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Footer);

/***/ }),

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Navbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Navbar */ "./components/Navbar.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Field__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Field */ "./components/Field.js");
/* harmony import */ var _Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Footer */ "./components/Footer.js");
/* harmony import */ var _Spacer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Spacer */ "./components/Spacer.js");
/* harmony import */ var _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../pages/Style.module.css */ "./pages/Style.module.css");
/* harmony import */ var _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_pages_Style_module_css__WEBPACK_IMPORTED_MODULE_6__);

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\components\\Layout.js";







const Layout = props => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
  children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
      children: "Laivanupotus"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 13
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
      rel: "stylesheet",
      href: "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css",
      integrity: "sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS",
      crossorigin: "anonymous"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("script", {
      src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
      integrity: "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo",
      crossorigin: "anonymous"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("script", {
      src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js",
      integrity: "sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut",
      crossorigin: "anonymous"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
      rel: "stylesheet",
      href: "App.css",
      type: "text/css"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 13,
    columnNumber: 9
  }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("body", {
    className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_6___default.a.body,
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("main", {
      className: _pages_Style_module_css__WEBPACK_IMPORTED_MODULE_6___default.a.main,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Navbar__WEBPACK_IMPORTED_MODULE_1__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 33,
        columnNumber: 13
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Spacer__WEBPACK_IMPORTED_MODULE_5__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 13
      }, undefined), " ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        class: "container",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          class: "row",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
            class: "col",
            children: "Pelaaja 1"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 21
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
            class: "col",
            children: "Pelaaja 2"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 40,
            columnNumber: 21
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 17
        }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          class: "row",
          children: ["   ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            class: "col",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Field__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 25
            }, undefined), " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 44,
            columnNumber: 21
          }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
            class: "col",
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Field__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 49,
              columnNumber: 25
            }, undefined), " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 48,
            columnNumber: 21
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 17
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 13
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Spacer__WEBPACK_IMPORTED_MODULE_5__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 56,
        columnNumber: 13
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Spacer__WEBPACK_IMPORTED_MODULE_5__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 57,
        columnNumber: 13
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Spacer__WEBPACK_IMPORTED_MODULE_5__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 13
      }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 13
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 29,
    columnNumber: 5
  }, undefined)]
}, void 0, true, {
  fileName: _jsxFileName,
  lineNumber: 10,
  columnNumber: 5
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/Navbar.js":
/*!******************************!*\
  !*** ./components/Navbar.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\components\\Navbar.js";

const Navbar = () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
  children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("nav", {
    class: "navbar navbar-expand-lg navbar-light bg-dark",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
      class: "navbar-brand text-light",
      href: "index.html",
      children: ["  ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h1", {
        children: "Main page"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 6,
        columnNumber: 64
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 6,
      columnNumber: 9
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
      class: "navbar-toggler",
      type: "button",
      "data-toggle": "collapse",
      "data-target": "#navbarNavDropdown",
      "aria-controls": "navbarNavDropdown",
      "aria-expanded": "false",
      "aria-label": "Toggle navigation",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
        class: "navbar-toggler-icon"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 9,
        columnNumber: 17
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 13
    }, undefined), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      class: "collapse navbar-collapse",
      id: "navbarNavDropdown",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("ul", {
        class: "navbar-nav",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("li", {
          class: "nav-item active",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
            class: "nav-link text-light",
            href: "#",
            children: "linkki esimerkki"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 16,
            columnNumber: 21
          }, undefined)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 15,
          columnNumber: 17
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 13
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 9
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 5,
    columnNumber: 5
  }, undefined)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 4,
  columnNumber: 1
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Navbar);

/***/ }),

/***/ "./components/Spacer.js":
/*!******************************!*\
  !*** ./components/Spacer.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\components\\Spacer.js";

const Spacer = () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
  style: {
    padding: "5rem",
    textAlign: "center",
    background: "rgb(255, 255, 255)",
    color: "rgb(255, 255, 255)"
  }
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 2,
  columnNumber: 5
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (Spacer);

/***/ }),

/***/ "./pages/Style.module.css":
/*!********************************!*\
  !*** ./pages/Style.module.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Exports
module.exports = {
	"body": "Style_body__h4jg8",
	"main": "Style_main__1I2QW",
	"table": "Style_table__1k_pL",
	"tr": "Style_tr__DuJjB",
	"td": "Style_td__9Fy4W"
};


/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");

var _jsxFileName = "C:\\Users\\apart\\OneDrive\\Ty\xF6p\xF6yt\xE4\\Ohjelmointi\\Webohj\\Next\\pages\\index.js";


const main = () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 5,
  columnNumber: 1
}, undefined);

/* harmony default export */ __webpack_exports__["default"] = (main);

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9GaWVsZC5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0Zvb3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL0xheW91dC5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL05hdmJhci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL1NwYWNlci5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9TdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovLy8uL3BhZ2VzL2luZGV4LmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQvaGVhZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIl0sIm5hbWVzIjpbIkZpZWxkIiwic3R5bGVzIiwidGFibGUiLCJ0ciIsInRkIiwiRm9vdGVyIiwiTGF5b3V0IiwicHJvcHMiLCJib2R5IiwibWFpbiIsIk5hdmJhciIsIlNwYWNlciIsInBhZGRpbmciLCJ0ZXh0QWxpZ24iLCJiYWNrZ3JvdW5kIiwiY29sb3IiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZBOztBQUVBLE1BQU1BLEtBQUssR0FBSSxtQkFFZjtBQUFPLFdBQVMsRUFBRUMsOERBQU0sQ0FBQ0MsS0FBekI7QUFBQSwwQkFDSTtBQUFJLGFBQVMsRUFBRUQsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURKLGVBY0k7QUFBSSxhQUFTLEVBQUVILDhEQUFNLENBQUNFLEVBQXRCO0FBQUEsaUNBQ0k7QUFBSSxlQUFTLEVBQUVGLDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURKLG9CQUVJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGSixlQUdJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFISixlQUlJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFKSixlQUtJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFMSixlQU1JO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFOSixlQU9JO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFQSixlQVFJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFSSixlQVNJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFUSixlQVVJO0FBQUksZUFBUyxFQUFFSCw4REFBTSxDQUFDRyxFQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFWSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFkSixlQTJCSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQTNCSixlQXdDSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQXhDSixlQXFESTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQXJESixlQWtFSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQWxFSixlQStFSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQS9FSixlQTRGSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQTVGSixlQXlHSTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQXpHSixlQXNISTtBQUFJLGFBQVMsRUFBRUgsOERBQU0sQ0FBQ0UsRUFBdEI7QUFBQSxpQ0FDSTtBQUFJLGVBQVMsRUFBRUYsOERBQU0sQ0FBQ0csRUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREosb0JBRUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUZKLGVBR0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhKLGVBSUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpKLGVBS0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxKLGVBTUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5KLGVBT0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBKLGVBUUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVJKLGVBU0k7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRKLGVBVUk7QUFBSSxlQUFTLEVBQUVILDhEQUFNLENBQUNHLEVBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQXRISjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFGQTs7QUE0SWVKLG9FQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlJQSxNQUFNSyxNQUFNLEdBQUcsbUJBQ2Y7QUFBQSx5QkFFQTtBQUFRLFNBQUssRUFBQyx1REFBZDtBQUFBLDJCQUVjO0FBQUssV0FBSyxFQUFDLFlBQVg7QUFBQSw2QkFDRTtBQUFJLGFBQUssRUFBQyw0Q0FBVjtBQUFBLGdDQUNFO0FBQUksZUFBSyxFQUFDLGtCQUFWO0FBQUEsaUNBQ0U7QUFBSSxpQkFBSyxFQUFDLGlCQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERixlQUlFO0FBQUksZUFBSyxFQUFDLDZCQUFWO0FBQUEsaUNBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURBOztBQXFCZUEscUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3JCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTUMsTUFBTSxHQUFJQyxLQUFELGlCQUVYO0FBQUEsMEJBR0kscUVBQUMsZ0RBQUQ7QUFBQSw0QkFDSTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFESixlQUlBO0FBQU0sU0FBRyxFQUFDLFlBQVY7QUFBdUIsVUFBSSxFQUFDLDBFQUE1QjtBQUF1RyxlQUFTLEVBQUMseUVBQWpIO0FBQTJMLGlCQUFXLEVBQUM7QUFBdk07QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFKQSxlQU1BO0FBQVEsU0FBRyxFQUFDLGtEQUFaO0FBQStELGVBQVMsRUFBQyx5RUFBekU7QUFBbUosaUJBQVcsRUFBQztBQUEvSjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5BLGVBUUE7QUFBUSxTQUFHLEVBQUMsMkVBQVo7QUFBd0YsZUFBUyxFQUFDLHlFQUFsRztBQUE0SyxpQkFBVyxFQUFDO0FBQXhMO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBUkEsZUFVQTtBQUFNLFNBQUcsRUFBQyxZQUFWO0FBQXVCLFVBQUksRUFBQyxTQUE1QjtBQUFzQyxVQUFJLEVBQUM7QUFBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFWQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFISixlQW1CQTtBQUFNLGFBQVMsRUFBRU4sOERBQU0sQ0FBQ08sSUFBeEI7QUFBQSwyQkFDSTtBQUFNLGVBQVMsRUFBRVAsOERBQU0sQ0FBQ1EsSUFBeEI7QUFBQSw4QkFHSSxxRUFBQywrQ0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUhKLGVBSUkscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFKSixvQkFNSTtBQUFLLGFBQUssRUFBQyxXQUFYO0FBQUEsZ0NBRUk7QUFBSyxlQUFLLEVBQUMsS0FBWDtBQUFBLGtDQUNJO0FBQUksaUJBQUssRUFBQyxLQUFWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQURKLGVBRUk7QUFBSSxpQkFBSyxFQUFDLEtBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZKLGVBT0k7QUFBSyxlQUFLLEVBQUMsS0FBWDtBQUFBLHlDQUNJO0FBQUssaUJBQUssRUFBQyxLQUFYO0FBQUEsb0NBQ0kscUVBQUMsOENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSx5QkFESjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREosZUFLSTtBQUFLLGlCQUFLLEVBQUMsS0FBWDtBQUFBLG9DQUNJLHFFQUFDLDhDQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEseUJBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUxKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFQSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBTkosZUEwQkkscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkExQkosZUEyQkkscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkEzQkosZUE0QkkscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkE1QkosZUE4QkkscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkE5Qko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQW5CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFGSjs7QUEyRGVILHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hFQSxNQUFNSSxNQUFNLEdBQUcsbUJBQ2Y7QUFBQSx5QkFDSTtBQUFLLFNBQUssRUFBQyw4Q0FBWDtBQUFBLDRCQUNJO0FBQUcsV0FBSyxFQUFDLHlCQUFUO0FBQW1DLFVBQUksRUFBQyxZQUF4QztBQUFBLG9DQUF1RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBdkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURKLGVBR1E7QUFBUSxXQUFLLEVBQUMsZ0JBQWQ7QUFBK0IsVUFBSSxFQUFDLFFBQXBDO0FBQTZDLHFCQUFZLFVBQXpEO0FBQW9FLHFCQUFZLG9CQUFoRjtBQUFxRyx1QkFBYyxtQkFBbkg7QUFBdUksdUJBQWMsT0FBcko7QUFBNkosb0JBQVcsbUJBQXhLO0FBQUEsNkJBQ0k7QUFBTSxhQUFLLEVBQUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFIUixlQU9JO0FBQUssV0FBSyxFQUFDLDBCQUFYO0FBQXNDLFFBQUUsRUFBQyxtQkFBekM7QUFBQSw2QkFDSTtBQUFJLGFBQUssRUFBQyxZQUFWO0FBQUEsK0JBRUk7QUFBSSxlQUFLLEVBQUMsaUJBQVY7QUFBQSxpQ0FDSTtBQUFHLGlCQUFLLEVBQUMscUJBQVQ7QUFBK0IsZ0JBQUksRUFBQyxHQUFwQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFQSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREE7O0FBdUJlQSxxRUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6QkEsTUFBTUMsTUFBTSxHQUFHLG1CQUNYO0FBQUssT0FBSyxFQUFFO0FBQ1pDLFdBQU8sRUFBRSxNQURHO0FBRVpDLGFBQVMsRUFBQyxRQUZFO0FBR1pDLGNBQVUsRUFBRSxvQkFIQTtBQUlaQyxTQUFLLEVBQUU7QUFKSztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFESjs7QUFhZUoscUVBQWYsRTs7Ozs7Ozs7Ozs7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDUEE7O0FBR0EsTUFBTUYsSUFBSSxHQUFHLG1CQUNiLHFFQUFDLDBEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFEQTs7QUFLZUEsbUVBQWYsRTs7Ozs7Ozs7Ozs7QUNSQSxzQzs7Ozs7Ozs7Ozs7QUNBQSxrRCIsImZpbGUiOiJwYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvaW5kZXguanNcIik7XG4iLCJpbXBvcnQgc3R5bGVzIGZyb20gJy4uL3BhZ2VzL1N0eWxlLm1vZHVsZS5jc3MnO1xyXG5cclxuY29uc3QgRmllbGQgID0gKCkgPT4gKFxyXG5cclxuPHRhYmxlIGNsYXNzTmFtZT17c3R5bGVzLnRhYmxlfT5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgMSovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgMiovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgMyovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgNCovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgNSovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgNiovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgNyovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgOCovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgOSovfSAgXHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xPC90ZD4gey8qc29sdSovfVxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4zPC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PiA0PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjU8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NjwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT43PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjg8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+OTwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4xMDwvdGQ+XHJcbiAgICA8L3RyPlxyXG5cclxuICAgIDx0ciBjbGFzc05hbWU9e3N0eWxlcy50cn0+IHsvKnJpdmkgMTAqL30gIFxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MTwvdGQ+IHsvKnNvbHUqL31cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjI8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MzwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT4gNDwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT41PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9PjY8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+NzwvdGQ+XHJcbiAgICAgICAgPHRkIGNsYXNzTmFtZT17c3R5bGVzLnRkfT44PC90ZD5cclxuICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZXMudGR9Pjk8L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzc05hbWU9e3N0eWxlcy50ZH0+MTA8L3RkPlxyXG4gICAgPC90cj5cclxuXHJcbiBcclxuXHJcbjwvdGFibGU+XHJcblxyXG5cclxuKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEZpZWxkOyIsImNvbnN0IEZvb3RlciA9ICgpID0+IChcclxuPGRpdj5cclxuXHJcbjxmb290ZXIgY2xhc3M9XCJwYWdlLWZvb3RlciBmb250LXNtYWxsIHVuaXF1ZS1jb2xvci1kYXJrIHB0LTQgYmctZGFya1wiPlxyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250YWluZXIgXCI+ICBcclxuICAgICAgICAgICAgICAgIDx1bCBjbGFzcz1cImxpc3QtdW5zdHlsZWQgbGlzdC1pbmxpbmUgdGV4dC1jZW50ZXIgcHktMlwiPlxyXG4gICAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJsaXN0LWlubGluZS1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGg1IGNsYXNzPVwibWItMSB0ZXh0LWxpZ2h0XCI+Rm9vdGVyPC9oNT5cclxuICAgICAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibGlzdC1pbmxpbmUtaXRlbSB0ZXh0LWxpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHA+dGVzdGk8L3A+XHJcbiAgICAgICAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgICAgICA8L3VsPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgPC9mb290ZXI+XHJcbiAgICAgICAgICBcclxuPC9kaXY+XHJcblxyXG4pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgRm9vdGVyOyIsImltcG9ydCBOYXZiYXIgZnJvbSAnLi9OYXZiYXInO1xyXG5pbXBvcnQgSGVhZCBmcm9tIFwibmV4dC9oZWFkXCI7XHJcbmltcG9ydCBGaWVsZCAgZnJvbSAnLi9GaWVsZCc7XHJcbmltcG9ydCBGb290ZXIgZnJvbSAnLi9Gb290ZXInO1xyXG5pbXBvcnQgU3BhY2VyIGZyb20gJy4vU3BhY2VyJztcclxuaW1wb3J0IHN0eWxlcyBmcm9tICcuLi9wYWdlcy9TdHlsZS5tb2R1bGUuY3NzJztcclxuXHJcbmNvbnN0IExheW91dCA9IChwcm9wcykgPT4gKFxyXG5cclxuICAgIDxkaXY+XHJcbiAgICAgICAgXHJcbiAgICAgICAgey8qVmFpaHRhYSBzaXZ1biB0aXRsZSovfVxyXG4gICAgICAgIDxIZWFkPlxyXG4gICAgICAgICAgICA8dGl0bGU+TGFpdmFudXBvdHVzPC90aXRsZT5cclxuICAgICAgICBcclxuICAgICAgICB7LypCb290c3RyYXAqL31cclxuICAgICAgICA8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cImh0dHBzOi8vc3RhY2twYXRoLmJvb3RzdHJhcGNkbi5jb20vYm9vdHN0cmFwLzQuMi4xL2Nzcy9ib290c3RyYXAubWluLmNzc1wiIGludGVncml0eT1cInNoYTM4NC1HSnpacUZHd2IxUVRUTjZ3eTU5ZmZGMUJ1R0pwTFNhOURrS01wMERnaU1EbTRpWU1qNzBnWldLWWJJNzA2dFdTXCIgY3Jvc3NvcmlnaW49XCJhbm9ueW1vdXNcIi8+XHJcbiAgICAgICAgey8qanF1ZXJ5Ki99XHJcbiAgICAgICAgPHNjcmlwdCBzcmM9XCJodHRwczovL2NvZGUuanF1ZXJ5LmNvbS9qcXVlcnktMy4zLjEuc2xpbS5taW4uanNcIiBpbnRlZ3JpdHk9XCJzaGEzODQtcThpL1grOTY1RHpPMHJUN2FiSzQxSlN0UUlBcVZnUlZ6cGJ6bzVzbVhLcDRZZlJ2SCs4YWJ0VEUxUGk2aml6b1wiIGNyb3Nzb3JpZ2luPVwiYW5vbnltb3VzXCI+PC9zY3JpcHQ+XHJcbiAgICAgICAgey8qamF2YXNjcmlwdCovfVxyXG4gICAgICAgIDxzY3JpcHQgc3JjPVwiaHR0cHM6Ly9jZG5qcy5jbG91ZGZsYXJlLmNvbS9hamF4L2xpYnMvcG9wcGVyLmpzLzEuMTQuNi91bWQvcG9wcGVyLm1pbi5qc1wiIGludGVncml0eT1cInNoYTM4NC13SEFpRmZSbE1GeTZpNVNSYXh2Zk9DaWZCVVF5MXhIZEoveW9pN0ZSTlhNUkJ1NVdIZFpZdTFoQTZaT2JsZ3V0XCIgY3Jvc3NvcmlnaW49XCJhbm9ueW1vdXNcIj48L3NjcmlwdD5cclxuICAgICAgICB7LypzdHlsZSBzaGVldCovfVxyXG4gICAgICAgIDxsaW5rIHJlbD1cInN0eWxlc2hlZXRcIiBocmVmPVwiQXBwLmNzc1wiIHR5cGU9XCJ0ZXh0L2Nzc1wiLz5cclxuXHJcbiAgICAgICAgXHJcbiAgICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAgICAgey8qSGVhZGVyKi99XHJcbiAgICA8Ym9keSBjbGFzc05hbWU9e3N0eWxlcy5ib2R5fT5cclxuICAgICAgICA8bWFpbiBjbGFzc05hbWU9e3N0eWxlcy5tYWlufT4gXHJcblxyXG4gICAgXHJcbiAgICAgICAgICAgIDxOYXZiYXIvPiBcclxuICAgICAgICAgICAgPFNwYWNlci8+IHsvKmJsb2traSBtaWxsw6QgYXNldGVsbHV0IHNpdnVuIGVsZW1lbnRpdCBwYXJlbW1pbiovfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+IFxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzPVwiY29sXCI+UGVsYWFqYSAxPC9oMT5cclxuICAgICAgICAgICAgICAgICAgICA8aDEgY2xhc3M9XCJjb2xcIj5QZWxhYWphIDI8L2gxPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvd1wiPiAgIHsvKmFzZXR0YWEga2VudMOkdCByaXZpaW4qL31cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxGaWVsZC8+IHsvKnBlbGlrZW50dMOkKi99IFxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8RmllbGQvPiB7LypwZWxpa2VudHTDpCovfSBcclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG5cclxuICAgICAgICAgICAgPFNwYWNlci8+XHJcbiAgICAgICAgICAgIDxTcGFjZXIvPlxyXG4gICAgICAgICAgICA8U3BhY2VyLz5cclxuICAgICAgICBcclxuICAgICAgICAgICAgPEZvb3Rlci8+XHJcblxyXG4gICAgICAgIDwvbWFpbj5cclxuICAgIDwvYm9keT5cclxuICAgIDwvZGl2PlxyXG4pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0OyIsIlxyXG5cclxuY29uc3QgTmF2YmFyID0gKCkgPT4gKFxyXG48ZGl2PlxyXG4gICAgPG5hdiBjbGFzcz1cIm5hdmJhciBuYXZiYXItZXhwYW5kLWxnIG5hdmJhci1saWdodCBiZy1kYXJrXCI+XHJcbiAgICAgICAgPGEgY2xhc3M9XCJuYXZiYXItYnJhbmQgdGV4dC1saWdodFwiIGhyZWY9XCJpbmRleC5odG1sXCI+ICA8aDE+TWFpbiBwYWdlPC9oMT48L2E+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwibmF2YmFyLXRvZ2dsZXJcIiB0eXBlPVwiYnV0dG9uXCIgZGF0YS10b2dnbGU9XCJjb2xsYXBzZVwiIGRhdGEtdGFyZ2V0PVwiI25hdmJhck5hdkRyb3Bkb3duXCIgYXJpYS1jb250cm9scz1cIm5hdmJhck5hdkRyb3Bkb3duXCIgYXJpYS1leHBhbmRlZD1cImZhbHNlXCIgYXJpYS1sYWJlbD1cIlRvZ2dsZSBuYXZpZ2F0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm5hdmJhci10b2dnbGVyLWljb25cIj48L3NwYW4+XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgIFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJjb2xsYXBzZSBuYXZiYXItY29sbGFwc2VcIiBpZD1cIm5hdmJhck5hdkRyb3Bkb3duXCI+XHJcbiAgICAgICAgICAgIDx1bCBjbGFzcz1cIm5hdmJhci1uYXZcIj5cclxuXHJcbiAgICAgICAgICAgICAgICA8bGkgY2xhc3M9XCJuYXYtaXRlbSBhY3RpdmVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YSBjbGFzcz1cIm5hdi1saW5rIHRleHQtbGlnaHRcIiBocmVmPVwiI1wiPmxpbmtraSBlc2ltZXJra2k8L2E+XHJcbiAgICAgICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICA8L25hdj5cclxuXHJcbjwvZGl2PlxyXG4pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgTmF2YmFyO1xyXG5cclxuIiwiY29uc3QgU3BhY2VyID0gKCkgPT4gKFxyXG4gICAgPGRpdiBzdHlsZT17eyBcclxuICAgIHBhZGRpbmc6IFwiNXJlbVwiLFxyXG4gICAgdGV4dEFsaWduOlwiY2VudGVyXCIsXHJcbiAgICBiYWNrZ3JvdW5kOiBcInJnYigyNTUsIDI1NSwgMjU1KVwiLFxyXG4gICAgY29sb3I6IFwicmdiKDI1NSwgMjU1LCAyNTUpXCJcclxuIH19PlxyXG4gICAgICAgXHJcbiAgICAgXHJcblxyXG4gICAgPC9kaXY+XHJcbik7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBTcGFjZXI7IiwiLy8gRXhwb3J0c1xubW9kdWxlLmV4cG9ydHMgPSB7XG5cdFwiYm9keVwiOiBcIlN0eWxlX2JvZHlfX2g0amc4XCIsXG5cdFwibWFpblwiOiBcIlN0eWxlX21haW5fXzFJMlFXXCIsXG5cdFwidGFibGVcIjogXCJTdHlsZV90YWJsZV9fMWtfcExcIixcblx0XCJ0clwiOiBcIlN0eWxlX3RyX19EdUpqQlwiLFxuXHRcInRkXCI6IFwiU3R5bGVfdGRfXzlGeTRXXCJcbn07XG4iLCJpbXBvcnQgTGF5b3V0IGZyb20gJy4uL2NvbXBvbmVudHMvTGF5b3V0JztcclxuXHJcblxyXG5jb25zdCBtYWluID0gKCkgPT4gKFxyXG48TGF5b3V0PiBcclxuPC9MYXlvdXQ+XHJcbik7XHJcbiAgICBcclxuZXhwb3J0IGRlZmF1bHQgbWFpbjsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L2hlYWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=